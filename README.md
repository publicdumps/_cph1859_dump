## full_oppo6771_17061-user 8.1.0 O11019 1571214530 release-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: CPH1859
- Brand: 
- Flavor: full_oppo6771_17061-user
- Release Version: 8.1.0
- Id: O11019
- Incremental: 1571214678
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: 480
- Fingerprint: 
- OTA version: 
- Branch: full_oppo6771_17061-user-8.1.0-O11019-1571214530-release-keys
- Repo: _cph1859_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
