#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:40955504:bf60b4b8b24d0286811ef4a61cb421502560cf69; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/bootdevice/by-name/boot:10995312:8c12ca377c47fd7232ea5aa0e9ae7a12a57a59c9 EMMC:/dev/block/platform/bootdevice/by-name/recovery bf60b4b8b24d0286811ef4a61cb421502560cf69 40955504 8c12ca377c47fd7232ea5aa0e9ae7a12a57a59c9:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
